package com.kubota.ofsll;

import com.kubota.ofsll.acd.AcdLoginPage;
import com.kubota.ofsll.acd.AcdNavigation;
import com.kubota.ofsll.acd.LoanApplicationPage;
import com.kubota.ofsll.db.entities.AcdEntity;
import com.kubota.ofsll.db.repos.AcdRepository;
import com.kubota.ofsll.fundingPage.FundingPage;
import com.kubota.ofsll.fundingPage.NavigateToOrigination;
import com.kubota.ofsll.loginpage.OfsllLoginPage;
import com.kubota.ofsll.pageclass.BasePage;
import com.kubota.ofsll.pageclass.CommonMethods;
import com.kubota.ofsll.pageclass.ServiceCalls;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.apache.commons.lang3.StringUtils;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.*;

@SpringBootTest
public class RunAppID extends BasePage {

    @Value("${ofsll.username}")
    String ofsllUserName;

    @Value("${ofsll.url}")
    String ofsllUrl;

    @Value("${ofsll.password}")
    String ofsllPassword;

    @Value("${acd.username}")
    String acdUserName;

    @Value("${acd.url}")
    String acdUrl;

    @Value("${acd.password}")
    String acdPassword;

    @Autowired
    AcdRepository acdRepository;

    @Autowired
    ServiceCalls serviceCalls;

    @Value("${acd.ksr.test.servlet.url}")
    private String servletUrl;

    StringBuilder kccErrorRecords = new StringBuilder();
    StringBuilder acdErrorRecords = new StringBuilder();
    StringBuilder ofsllErrorRecords = new StringBuilder();
    StringBuilder kccProcessedRecords = new StringBuilder();
    StringBuilder acdProcessedRecords = new StringBuilder();
    StringBuilder ofsllProcessedRecords = new StringBuilder();

    @BeforeClass
    public void setupClassName(ITestContext context) {
        context.getCurrentXmlTest().getSuite().setDataProviderThreadCount(10);
        context.getCurrentXmlTest().getSuite().setPreserveOrder(false);
    }

    @Test(dataProvider="Funding", groups = "ofsll")
    public void runner(String fileNumber)
    {
        boolean xmlFetched = false;
        WebDriver driver = null;
        String xml = null;
        try
        {
            AcdEntity acdEntity = acdRepository.findLatestById(Integer.parseInt(fileNumber));
            xml = acdEntity.getXML_MSG();
            xmlFetched = true;
        }
        catch (Exception e){
            kccErrorRecords.append("Not able to fetch XML from database: "+fileNumber+"\n");
            Assert.fail("Unable to fetch xml: "+fileNumber+"\n");
        }
        try
        {
            if(xmlFetched==true)
            {
                String docID = serviceCalls.writeToFileAndGetDocId(xml, servletUrl);
                if(docID==null)
                {
                    acdErrorRecords.append("Input file number :"+fileNumber+"---"+" Unable to generate application ID"+"\n");
                    Assert.fail("Input file number :"+fileNumber+"---"+" Unable to generate application ID");
                }
                else
                {
                    acdValidation(driver, docID, fileNumber);
                }

            }
        }
        catch (Exception e){

        }
    }

    private WebDriver valdateKcc(File fileNumber, WebDriver driver, String docID) {
        String file = fileNumber.getName().replace(".xml","");
        if(docID!=null)
        {
            kccProcessedRecords.append("Xml file "+file+ " ---- generated File Number --- "+docID+"\n");
            driver = acdValidation(driver, docID, file);
        }
        else
        {
            kccErrorRecords.append("Unable to generate the app ID from servlet: "+file+"\n");
        }
        return driver;
    }

    private WebDriver acdValidation(WebDriver driver, String docID, String fileNumber) {
        try
        {
            driver = initializeDriver(acdUrl);
            acdLoginSteps(driver);
            navigateToAcdPage(docID, driver);
            if(verifyACDStatus(docID,driver)==true)
            {
                approveAcd(driver, acdErrorRecords);
                acdProcessedRecords.append("Input file number :"+fileNumber+"---"+docID + " File successfully processed in ACD"+"\n");
            }
            else
            {
                acdProcessedRecords.append("Input file number :"+fileNumber+"---"+docID + " File status is not eligible for approval"+"\n");
            }
            Thread.sleep(7000);
            ofsllValidation(driver, docID, fileNumber);
        }
        catch (Exception e){
            acdErrorRecords.append("Input file number :"+fileNumber+"---"+" Error processing file: "+docID+"\n");
            takeScreenshot(driver, fileNumber);
            driver.quit();
            Assert.fail("Input file number :"+fileNumber+"---"+" Unable to process application in ACD: "+docID);
        }
        return driver;
    }

    private void ofsllValidation(WebDriver driver, String docID, String fileNumber) {
        String fund = null;
        try
        {
            navigateToOfsll(driver, ofsllUserName, ofsllUserName);
            navigateToFunding(driver);
            fundingPageActions(docID, driver);
            FundingPage fundingPage = new FundingPage();
            fundingPage.setDriver(driver);
            fund = fundingPage.verifyFund();
            validateFunding(driver, docID, fund);
            ofsllProcessedRecords.append("Input file number :"+fileNumber+"---"+docID + " File successfully processed in Ofsll"+"\n");
            driver.quit();
        }
        catch (Exception e){
            ofsllErrorRecords.append("Input file number :"+fileNumber+"---"+" Error processing file: "+docID+"\n");
            takeScreenshot(driver, fileNumber);
            driver.quit();
            Assert.fail("Input file number :"+fileNumber+"---"+" Issue with Ofsll: "+docID);
        }
    }

    private void validateFunding(WebDriver driver, String docID, String fund) {
        if(!(StringUtils.equalsIgnoreCase("APPROVED - FUNDED",fund)))
        {
            ofsllErrorRecords.append("Application did not get funded: "+docID+"\n");
            driver.quit();
            Assert.fail("Unable to fund application: "+docID);
        }
    }

    @AfterClass
    public void insertData() throws Exception{

        File acdError = new File("src/main/resources/testdata/errorrecords/AcdErrorRecords.txt");
        FileUtils.writeStringToFile(acdError, acdErrorRecords.toString(), String.valueOf(StandardCharsets.UTF_8));
        File kccError = new File("src/main/resources/testdata/errorrecords/KccErrorRecords.txt");
        FileUtils.writeStringToFile(kccError, kccErrorRecords.toString(), String.valueOf(StandardCharsets.UTF_8));
        File ofsllError = new File("src/main/resources/testdata/errorrecords/OfsllErrorRecords.txt");
        FileUtils.writeStringToFile(ofsllError, ofsllErrorRecords.toString(), String.valueOf(StandardCharsets.UTF_8));
        File acdSuccess = new File("src/main/resources/testdata/successrecords/AcdRecords.txt");
        FileUtils.writeStringToFile(acdSuccess, acdProcessedRecords.toString(), String.valueOf(StandardCharsets.UTF_8));
        File kccSuccess = new File("src/main/resources/testdata/successrecords/KccRecords.txt");
        FileUtils.writeStringToFile(kccSuccess, kccProcessedRecords.toString(), String.valueOf(StandardCharsets.UTF_8));
        File ofsllSucess = new File("src/main/resources/testdata/successrecords/OfsllRecords.txt");
        FileUtils.writeStringToFile(ofsllSucess, ofsllProcessedRecords.toString(), String.valueOf(StandardCharsets.UTF_8));
    }

    private void navigateToFunding(WebDriver driver) throws Exception{
        NavigateToOrigination navigateToOrigination = new NavigateToOrigination();
        navigateToOrigination.setDriver(driver);
        navigateToOrigination.clickOrigination();
    }

    private void approveAcd(WebDriver driver, StringBuilder builder) throws Exception{
        LoanApplicationPage loanApplicationPage = new LoanApplicationPage();
        loanApplicationPage.setDriver(driver);
        loanApplicationPage.selectApprove();
        loanApplicationPage.clickSubmit();
        loanApplicationPage.clickDecisionSubmit();
    }

    private void navigateToAcdPage(String fileNumber, WebDriver driver) throws Exception {
        AcdNavigation acdNavigation = new AcdNavigation();
        acdNavigation.setDriver(driver);
        acdNavigation.navigateToACD();
    }

    public boolean verifyACDStatus(String fileNumber, WebDriver driver) throws Exception{
        AcdNavigation acdNavigation = new AcdNavigation();
        acdNavigation.setDriver(driver);
        if(StringUtils.equalsIgnoreCase("lease",System.getProperty("application.type")))
        {
            return acdNavigation.searchForLeaseAcd(fileNumber);
        }
        else
        {
            return acdNavigation.searchForACD(fileNumber);
        }
    }

    private void acdLoginSteps(WebDriver driver) throws Exception {
        AcdLoginPage acdLoginPage = new AcdLoginPage();
        acdLoginPage.setDriver(driver);
        acdLoginPage.enterUserName(acdUserName);
        acdLoginPage.enterPassword(acdPassword);
        acdLoginPage.clickSubmit(acdErrorRecords);
    }

    private WebDriver initializeDriver(String url) {
        WebDriver driver = getDriver();
        driver.get(url);
        driver.manage().window().maximize();
        return driver;
    }

    private void takeScreenshot(WebDriver driver, String fileNumber) {
        CommonMethods commonMethods = new CommonMethods();
        commonMethods.setDriver(driver);
        commonMethods.takeScreenshot(fileNumber);
    }


    private void fundingPageActions(String fileNumber, WebDriver driver) throws Exception{
        FundingPage fundingPage = new FundingPage();
        fundingPage.setDriver(driver);
        fundingPage.clickFundingLink();
        fundingPage.enterAppId(fileNumber);
        fundingPage.clickAppSubmit();
        fundingPage.clickContractTab();
        fundingPage.clickItemizationTab();
        fundingPage.getAndUpdateAmount();
    }

    private void navigateToOfsll(WebDriver driver, String userName, String password) throws Exception{
        OfsllLoginPage ofsllLoginPage = new OfsllLoginPage();
        ofsllLoginPage.setDriver(driver);
        ofsllLoginPage.navigateToOsfll(ofsllUrl);
        ofsllLoginPage.enterUserName(ofsllUserName);
        ofsllLoginPage.enterPassword(ofsllPassword);
        ofsllLoginPage.clickSignIn();
    }

    @DataProvider(name="Funding", parallel = true)
    public Iterator<Object[]> approvalFunding()
    {
        List<String> stringlist = new ArrayList<String>();
        File inputFile = new File("src/main/resources/fileNumbers.txt");
        try
        {
            Scanner scanner = new Scanner(inputFile);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if(!StringUtils.isEmpty(line) && !line.contains("null")) {
                    stringlist.add(line.trim());
                }
            }
        }
        catch (Exception e){e.printStackTrace();}
        Collection<Object[]> dp = new ArrayList<Object[]>();
        for(String newString : stringlist)
        {
            dp.add(new Object[]{newString});
        }
        return dp.iterator();
    }

    @DataProvider(name="pushToACD", parallel = true)
    public Iterator<Object[]> pushToACD()
    {
        List<String> stringlist = new ArrayList<String>();
        try
        {
            File inputFile = new File("src/main/resources/fileNumbers.txt");
            Scanner scanner = new Scanner(inputFile);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if(!StringUtils.isEmpty(line) && !line.contains("null")) {
                    stringlist.add(line.trim());
                }
            }
        }
        catch (Exception e){e.printStackTrace();}
        Collection<Object[]> dp = new ArrayList<Object[]>();
        for(String newString : stringlist)
        {
            dp.add(new Object[]{newString});
        }
        return dp.iterator();
    }

}
