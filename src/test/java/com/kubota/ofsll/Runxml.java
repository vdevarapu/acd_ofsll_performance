package com.kubota.ofsll;

import com.kubota.ofsll.acd.AcdLoginPage;
import com.kubota.ofsll.acd.AcdNavigation;
import com.kubota.ofsll.acd.LoanApplicationPage;
import com.kubota.ofsll.db.entities.AcdEntity;
import com.kubota.ofsll.db.repos.AcdRepository;
import com.kubota.ofsll.fundingPage.FundingPage;
import com.kubota.ofsll.fundingPage.NavigateToOrigination;
import com.kubota.ofsll.loginpage.OfsllLoginPage;
import com.kubota.ofsll.pageclass.BasePage;
import com.kubota.ofsll.pageclass.ServiceCalls;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StringUtils;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootTest
public class Runxml extends BasePage  {

    @Value("${ofsll.username}")
    String ofsllUserName;

    @Value("${ofsll.url}")
    String ofsllUrl;

    @Value("${ofsll.password}")
    String ofsllPassword;

    @Value("${acd.username}")
    String acdUserName;

    @Value("${acd.url}")
    String acdUrl;

    @Value("${acd.password}")
    String acdPassword;

    @Autowired
    AcdRepository acdRepository;

    @Value("${acd.ksr.test.servlet.url}")
    private String servletUrl;

    StringBuilder kccErrorRecords = new StringBuilder();
    StringBuilder acdErrorRecords = new StringBuilder();
    StringBuilder ofsllErrorRecords = new StringBuilder();
    StringBuilder kccProcessedRecords = new StringBuilder();
    StringBuilder acdProcessedRecords = new StringBuilder();
    StringBuilder ofsllProcessedRecords = new StringBuilder();

    @BeforeClass
    public void setupClassName(ITestContext context) {
        context.getCurrentXmlTest().getSuite().setDataProviderThreadCount(10);
        context.getCurrentXmlTest().getSuite().setPreserveOrder(false);
    }

    @Test(dataProvider="pushToACD", groups = "ofsll")
    public void runner(File fileNumber)
    {
        WebDriver driver = null;
        try
        {
            String fileContent = FileUtils.readFileToString(fileNumber,String.valueOf(Charset.forName("UTF-8")));
            ServiceCalls serviceCalls = new ServiceCalls();
            String docID = serviceCalls.writeToFileAndGetDocId(fileContent, servletUrl);
            System.out.println(docID);
            driver = valdateKcc(fileNumber, driver, docID);
        }
        catch (Exception e){
            kccErrorRecords.append("Not able to fetch XML from database: "+fileNumber+"\n");
        }
    }

    private WebDriver valdateKcc(File fileNumber, WebDriver driver, String docID) {
        String file = fileNumber.getName().replace(".xml","");
        if(docID!=null)
        {
            kccProcessedRecords.append("Xml file "+file+ " ---- generated File Number --- "+docID+"\n");
            driver = acdValidation(driver, docID);
        }
        else
        {
            kccErrorRecords.append("Unable to generate the app ID from servlet: "+file+"\n");
        }
        return driver;
    }

    private WebDriver acdValidation(WebDriver driver, String docID) {
        try
        {
            driver = initializeDriver(acdUrl);
            acdLoginSteps(driver);
            navigateToAcdPage(docID, driver);
            if(verifyACDStatus(docID,driver)==true)
            {
                approveAcd(driver, acdErrorRecords);
                acdProcessedRecords.append(docID + " File successfully processed in ACD"+"\n");
            }
            else
            {
                acdProcessedRecords.append(docID + " File status is not eligible for approval"+"\n");
            }
            //Thread.sleep(10000);
            ofsllValidation(driver, docID);
        }
        catch (Exception e){
            acdErrorRecords.append("Error processing file: "+docID+"\n");
            driver.quit();
            Assert.fail("Error processing file "+docID);
        }
        return driver;
    }

    private void ofsllValidation(WebDriver driver, String docID) {
        try
        {
            navigateToOfsll(driver, ofsllUserName, ofsllUserName);
            navigateToFunding(driver);
            fundingPageActions(docID, driver);
            ofsllProcessedRecords.append(docID + " File successfully processed in Ofsll"+"\n");
            driver.quit();
        }
        catch (Exception e){
            ofsllErrorRecords.append("Error processing file: "+docID+"\n");
            driver.quit();
            Assert.fail("Error processing file "+docID);
        }
    }

    @AfterClass
    public void insertData() throws Exception{

        File acdError = new File("src/main/resources/testdata/errorrecords/AcdErrorRecords.txt");
        FileUtils.writeStringToFile(acdError, acdErrorRecords.toString(), String.valueOf(StandardCharsets.UTF_8));
        File kccError = new File("src/main/resources/testdata/errorrecords/KccErrorRecords.txt");
        FileUtils.writeStringToFile(kccError, kccErrorRecords.toString(), String.valueOf(StandardCharsets.UTF_8));
        File ofsllError = new File("src/main/resources/testdata/errorrecords/OfsllErrorRecords.txt");
        FileUtils.writeStringToFile(ofsllError, ofsllErrorRecords.toString(), String.valueOf(StandardCharsets.UTF_8));
        File acdSuccess = new File("src/main/resources/testdata/successrecords/AcdRecords.txt");
        FileUtils.writeStringToFile(acdSuccess, acdProcessedRecords.toString(), String.valueOf(StandardCharsets.UTF_8));
        File kccSuccess = new File("src/main/resources/testdata/successrecords/KccRecords.txt");
        FileUtils.writeStringToFile(kccSuccess, kccProcessedRecords.toString(), String.valueOf(StandardCharsets.UTF_8));
        File ofsllSucess = new File("src/main/resources/testdata/successrecords/OfsllRecords.txt");
        FileUtils.writeStringToFile(ofsllSucess, ofsllProcessedRecords.toString(), String.valueOf(StandardCharsets.UTF_8));
    }

    private void navigateToFunding(WebDriver driver) throws Exception{
        NavigateToOrigination navigateToOrigination = new NavigateToOrigination();
        navigateToOrigination.setDriver(driver);
        navigateToOrigination.clickOrigination();
    }

    private void approveAcd(WebDriver driver, StringBuilder builder) throws Exception{
        LoanApplicationPage loanApplicationPage = new LoanApplicationPage();
        loanApplicationPage.setDriver(driver);
        loanApplicationPage.selectApprove();
        loanApplicationPage.clickSubmit();
        loanApplicationPage.clickDecisionSubmit();
    }

    private void navigateToAcdPage(String fileNumber, WebDriver driver) throws Exception {
        AcdNavigation acdNavigation = new AcdNavigation();
        acdNavigation.setDriver(driver);
        acdNavigation.navigateToACD();
        //acdNavigation.searchForACD(fileNumber);
    }

    public boolean verifyACDStatus(String fileNumber, WebDriver driver) throws Exception{
        AcdNavigation acdNavigation = new AcdNavigation();
        acdNavigation.setDriver(driver);
        return acdNavigation.searchForACD(fileNumber);
    }

    private void acdLoginSteps(WebDriver driver) throws Exception {
        AcdLoginPage acdLoginPage = new AcdLoginPage();
        acdLoginPage.setDriver(driver);
        acdLoginPage.enterUserName(acdUserName);
        acdLoginPage.enterPassword(acdPassword);
        acdLoginPage.clickSubmit(acdErrorRecords);
    }

    private WebDriver initializeDriver(String url) {
        WebDriver driver = getDriver();
        driver.get(url);
        driver.manage().window().maximize();
        return driver;
    }


    private void fundingPageActions(String fileNumber, WebDriver driver) throws Exception{
        FundingPage fundingPage = new FundingPage();
        fundingPage.setDriver(driver);
        fundingPage.clickFundingLink();
        fundingPage.enterAppId(fileNumber);
        fundingPage.clickAppSubmit();
        fundingPage.clickContractTab();
        fundingPage.clickItemizationTab();
        fundingPage.getAndUpdateAmount();
    }

    private void navigateToOfsll(WebDriver driver, String userName, String password) throws Exception{
        OfsllLoginPage ofsllLoginPage = new OfsllLoginPage();
        ofsllLoginPage.setDriver(driver);
        ofsllLoginPage.navigateToOsfll(ofsllUrl);
        ofsllLoginPage.enterUserName(ofsllUserName);
        ofsllLoginPage.enterPassword(ofsllPassword);
        ofsllLoginPage.clickSignIn();
    }

    @DataProvider(name="Funding", parallel = true)
    public Iterator<Object[]> approvalFunding()
    {
        List<File> filesInFolder = new ArrayList<File>();
        try
        {
            Stream<Path> stream = Files.walk(Paths.get("src\\main\\resources\\serice-logs"));
            filesInFolder = Files.walk(Paths.get("src\\main\\resources\\serice-logs"))
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .collect(Collectors.toList());

        }
        catch (Exception e){e.printStackTrace();}
        Collection<Object[]> dp = new ArrayList<Object[]>();
        for(File newString : filesInFolder)
        {
            dp.add(new Object[]{newString});
        }
        return dp.iterator();
    }

    @DataProvider(name="pushToACD", parallel = true)
    public Iterator<Object[]> pushToACD()
    {
        List<String> stringlist = new ArrayList<String>();
        List<File> filesInFolder = new ArrayList<File>();
       try
        {
            filesInFolder = Files.walk(Paths.get("src/main/resources/testdata/applicationxmls"))
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .collect(Collectors.toList());

        }
        catch (Exception e){}
        Collection<Object[]> dp = new ArrayList<Object[]>();
        for(File newString : filesInFolder)
        {
            dp.add(new Object[]{newString});
        }
        return dp.iterator();
    }

}
