package com.kubota.ofsll.pageclass;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import java.nio.charset.StandardCharsets;

@Component
public class ServiceCalls {

    @Value("${acd.ksr.test.servlet.url}")
    private String servletUrl;

    public String writeToFileAndGetDocId(String data, String servletUrl) throws Exception {

        String convertedData = getMassagedData(data);
        String docId = null;

        try {
            String response = sendReqToGetResponse(convertedData, servletUrl);
            if(StringUtils.isNotBlank(response)) {
                docId = getDocumentIdFromResponse(response);
            }
        } catch (IOException e) {

        }
        return docId;
    }

    private String getMassagedData(String data) throws Exception {
        String resultStr = null;
        try
        {
            StringWriter writer = removeDocId(data);
            resultStr = putRandomPartyId(writer.toString());
        }
        catch (Exception e){
            Assert.fail(e.getMessage());
        }

        return resultStr;
    }

    private StringWriter removeDocId(String data) throws ParserConfigurationException, SAXException, IOException, TransformerException {
        Document doc = setDocumentBuilder(data);
        StringWriter writer = new StringWriter();
        NodeList DocumentId = doc.getElementsByTagName("DocumentId");
        for (int i = 0; i < DocumentId.getLength(); i++) {
            Node node = DocumentId.item(i);
            System.out.println(node.getNodeName());
            node.getParentNode().removeChild(node);
        }
        DOMSource domSource = new DOMSource(doc);
        StreamResult result = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.transform(domSource, result);
        return writer;
    }

    private Document setDocumentBuilder(String oracleReq) throws ParserConfigurationException, SAXException, IOException
    {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        InputStream in = IOUtils.toInputStream(oracleReq, "UTF-8");
        return docBuilder.parse(in);
    }

    private String putRandomPartyId(String oracleReq) throws Exception {

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        InputStream in = IOUtils.toInputStream(oracleReq, "UTF-8");
        Document doc = docBuilder.parse(in);

        NodeList partyIdList = doc.getElementsByTagName("PartyId");
        for (int count = 0; count < partyIdList.getLength(); count++) {
            Node partyIdNode = partyIdList.item(count);
            if (partyIdNode.getTextContent().length() == 9 && org.apache.commons.lang3.StringUtils.isNumeric(partyIdNode.getTextContent())) {
                partyIdNode.setTextContent(randomNumbGen().toString());
            }
        }
        DOMSource domSource = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.transform(domSource, result);
        String convertedStr = writer.toString();

        return convertedStr;
    }


    private static Long randomNumbGen() {
        while (true) {
            long numb = (long) (Math.random() * 100000000 * 1000000);
            if (String.valueOf(numb).length() == 9)
                return numb;
        }
    }


    public String sendReqToGetResponse(String data, String servletUrl) throws Exception {

        HttpClient httpClient = new HttpClient();
        PostMethod postMethod = new PostMethod(servletUrl);
        postMethod.addParameter("xml", data);
        String resp = null;
        httpClient.executeMethod(postMethod);
        resp = postMethod.getResponseBodyAsString();
        return resp;
    }

    private String getDocumentIdFromResponse(String response) throws Exception {
        String docId = null;

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        InputStream in = IOUtils.toInputStream(response, "UTF-8");
        Document doc;
        doc = docBuilder.parse(in);
        Node docIdNode = doc.getElementsByTagName("DocumentId").item(0);
        docId = docIdNode.getTextContent();

        return docId;

    }
}
