package com.kubota.ofsll.pageclass;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.context.annotation.Scope;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;


public class BasePage extends AbstractTestNGSpringContextTests {

    public static WebDriver getDriver()
    {
        System.setProperty("webdriver.chrome.driver","src/main/resources/driver/chromedriver.exe");
        return new ChromeDriver();
    }
}
