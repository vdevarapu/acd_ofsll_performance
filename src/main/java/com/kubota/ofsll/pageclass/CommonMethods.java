package com.kubota.ofsll.pageclass;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.testng.Assert;
import org.testng.Reporter;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.kubota.ofsll.pageclass.BasePage.getDriver;

@Component
@Scope("prototype")
public class CommonMethods {

    public WebDriver getdver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    WebDriver driver;

    public void waitForLoadClose()
    {
        for(int i=0;i<1000;i++)
        {
            try
            {
                Thread.sleep(300);
                if(driver.findElement(By.xpath("//span[@id='pt1:pt_statInd']/img[@alt='Idle']")).isDisplayed())
                {
                    Thread.sleep(300);
                    break;
                }
            }
            catch (Exception e){
                //do nothing
            }
        }
    }

    public void sendTebKeys(By element)
    {
        try
        {
            driver.findElement(element).click();
            Thread.sleep(200);
            driver.findElement(element).sendKeys(Keys.TAB);
        }
        catch (Exception e){

        }
    }

    public void elementEnterText(By element, String text) throws Exception
    {

        driver.findElement(element).click();
        Thread.sleep(300);
        driver.findElement(element).sendKeys(text);

    }

    public void elementClick(By element) throws Exception
    {
        waitForElement(element);
        driver.findElement(element).click();
        Thread.sleep(300);
    }

    public String getText(By element)
    {
        /*Actions actions = new Actions(getDriver());
        actions.moveToElement(getDriver().findElement(element)).build().perform();*/
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)driver;
        javascriptExecutor.executeScript("arguments[0].scrollIntoView();", driver.findElement(element));
        return driver.findElement(element).getText();
    }

    public void waitForElement(By element)
    {
        for(int i = 1; i<100; i++)
        {
            try
            {
                if(driver.findElement(element).isDisplayed())
                {
                    Thread.sleep(300);
                    break;
                }
            }
            catch (Exception e){}
        }
    }

    public void waitForStringElement(String element)
    {
        for(int i = 1; i<100; i++)
        {
            try
            {
                if(driver.findElement(By.xpath(element)).isDisplayed())
                {
                    Thread.sleep(300);
                    break;
                }
            }
            catch (Exception e){}
        }
    }

    public void takeScreenshot(String filename)  {
        try {
            String timeStamp;
            File screenShotName;
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
            screenShotName = new File("build/reports/tests/test/"+filename+"_"+timeStamp+".png");
            FileUtils.copyFile(scrFile, screenShotName);
            String path = "<img src='"+screenShotName.getAbsolutePath()+"'>";
            Reporter.log("<a href='"+ screenShotName.getAbsolutePath() + "'> <img src='"+ screenShotName.getAbsolutePath() + "' height='100' width='100'/> </a>");
            //Reporter.log(path);
        }
        catch (Exception e){}
    }
}
