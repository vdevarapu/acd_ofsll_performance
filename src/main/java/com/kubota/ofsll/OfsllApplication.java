package com.kubota.ofsll;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OfsllApplication {

	public static void main(String[] args) {
		SpringApplication.run(OfsllApplication.class, args);
	}

}
