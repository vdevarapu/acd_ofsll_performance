package com.kubota.ofsll.acd;

import com.kubota.ofsll.pageclass.CommonMethods;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Component;
import org.testng.Reporter;

@Component
public class LoanApplicationPage extends CommonMethods {

    public WebDriver getdver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
        super.setDriver(driver);
    }

    WebDriver driver;

    private static final By approvedOption = By.xpath("//select[@name='decision']/option[@value='Approved']");
    private static final By submitButton = By.id("submit_o");
    private static final By detailsHeading = By.xpath("//td[@class='maintitle'][text()='Decision Details - Approved']");
    private static final By approvedHeading = By.xpath("//td[@class='maintitle'][text()='Decision Details - Approved Verification']");
    private static final By reassignLink = By.xpath("//*[@id='no_print']/a[text()='reassign']");
    private static final By reassignHeading = By.xpath("//*[@id='pagetitle']/tbody/tr[1]/td[text()='Reassign']");
    private static final By reassignname = By.xpath("//select[@name='user']/option[@value='acdkubdtr']");
    private static final By satifactoryCheckBox = By.xpath("//tr[2]/td/table/tbody/tr[1]/td[1]/input[@name='selIds']");
    private static final By confirmButton = By.xpath("//img[@alt='Confirm and Return to Loan Application']");
    private static final By confirmText = By.xpath("//td[@class='maintitle'][text()='Loan Application - Decision Reasons Summary']");
    private static final By approvedText = By.xpath("//*[@id='applicantinfo']/tbody/tr[1]/td[2]/span/p/b[text()='Status: Approved']");


    public void reassignFile() throws Exception
    {
        elementClick(reassignLink);
        waitForElement(reassignHeading);
        elementClick(reassignname);
        elementClick(submitButton);
    }

    public void selectApprove() throws Exception
    {
        reassignFile();
        waitForElement(approvedOption);
        elementClick(approvedOption);
        Reporter.log("Selected Approve option");
    }

    public void clickSubmit() throws Exception
    {
        elementClick(submitButton);
        waitForElement(detailsHeading);
        Reporter.log("Click on the Approve Submit button");
    }

    public void clickDecisionSubmit() throws Exception
    {
        if(!(driver.findElement(satifactoryCheckBox)).isSelected())
        {
            driver.findElement(satifactoryCheckBox).click();
            Thread.sleep(300);
        }
        elementClick(submitButton);
        waitForElement(approvedHeading);
        driver.findElement(approvedHeading).click();
        Reporter.log("Clicked on the Decision submit button");
        elementClick(confirmButton);
        waitForElement(confirmText);
        Reporter.log("Clicked on the Confirm request button");
        elementClick(approvedText);
    }


}
