package com.kubota.ofsll.acd;


import com.kubota.ofsll.pageclass.CommonMethods;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Component;
import org.testng.Reporter;

import java.util.Map;

@Component
public class AcdLoginPage extends CommonMethods {

    public WebDriver getdver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
        super.setDriver(driver);
    }

    WebDriver driver;

    private static final By userNameField = By.id("j_username");
    private static final By passwordField = By.id("j_password");
    private static final By submitButton = By.xpath("//a[@href='javascript:document.loginForm.submit()']");
    private static final By searchField = By.xpath("//input[@class='search_results']");
    private static final By searchButton = By.id("search");
    private static final By moreOptions = By.xpath("//a[text()='More Options']");
    private static final By leaseApplication = By.xpath("//select[@name='installment']/option[@value='L']");
    public static final String SEARCH_RESULTS = "//td[@class='reportTD']/a[text()='";

    public void launchACD(String url)
    {
        driver.get(url);
        //driver.manage().window().maximize();
    }

    public void enterUserName(String userName) throws Exception
    {
        elementEnterText(userNameField,userName);
        Reporter.log("Entered ACD user name");
        sendTebKeys(userNameField);
    }

    public void enterPassword(String password) throws Exception
    {
        elementEnterText(passwordField, password);
        Reporter.log("Entered ACD password");
        sendTebKeys(passwordField);
    }

    public void clickSubmit(StringBuilder errorString) throws Exception
    {
        elementClick(submitButton);
        Reporter.log("Clicked on the ACD Sign In button");
    }

}
