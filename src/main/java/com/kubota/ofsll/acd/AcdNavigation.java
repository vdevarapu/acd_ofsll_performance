package com.kubota.ofsll.acd;

import com.kubota.ofsll.pageclass.CommonMethods;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Component;
import org.testng.Reporter;

import java.util.Map;

@Component
public class AcdNavigation extends CommonMethods {

    public WebDriver getdver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
        super.setDriver(driver);
    }

    WebDriver driver;

    private static final By financeMenuBar = By.xpath("//div[@class='menuBar']/a[text()='Finance']");
    private static final By acdMenuItem = By.xpath("//a[@class='menuItem'][text()='Automatic Credit Decision']");
    private static final By searchField = By.xpath("//input[@class='search_results']");

    private static final By acdStatus = By.xpath("//form[@action='/ACDWeb/searchResultsPrepare.do']/table/tbody/tr/td/table/tbody/tr[2]/td[2]/table[6]/tbody/tr/td[6]");
    private static final By searchButton = By.id("search");
    public String searchResults = "//td[@class='reportTD']/a[text()='";
    private static final By moreOptions = By.xpath("//a[text()='More Options']");
    private static final By leaseApplication = By.xpath("//select[@name='installment']/option[@value='L']");
    public static final String SEARCH_RESULTS = "//td[@class='reportTD']/a[text()='";

    public void navigateToACD() throws Exception
    {
        driver.navigate().to("https://stg.kubotalink.com" + "/ACDWeb/myApplicationsPrepare.do");
    }

    public void clickFinance() throws Exception
    {
        waitForElement(financeMenuBar);
        elementClick(financeMenuBar);
        Reporter.log("Clicked on the Finance tab");
    }

    public void clickACD() throws Exception
    {
        waitForElement(acdMenuItem);
        elementClick(acdMenuItem);
        Reporter.log("Clicked on the ACD Menu Item");
    }

    public boolean searchForACD(String fileNumber) throws Exception
    {
        boolean status = false;
        waitForElement(searchField);
        elementEnterText(searchField,fileNumber);
        elementClick(searchButton);
        Reporter.log("Searched for file Number "+ fileNumber);
        waitForStringElement(searchResults+fileNumber+"']");
        if(driver.findElement(By.xpath(searchResults+fileNumber+"']")).isDisplayed())
        {
            String appStatus = driver.findElement(acdStatus).getText();
            status = verifyStatus(fileNumber, appStatus);
        }
        return status;
    }

    public boolean searchForLeaseAcd(String fileNumber) throws Exception
    {
        boolean status = false;
        waitForElement(searchField);
        elementEnterText(searchField,fileNumber);
        elementClick(moreOptions);
        waitForElement(leaseApplication);
        elementClick(leaseApplication);
        elementClick(searchButton);
        waitForStringElement(searchResults+fileNumber+"']");
        if(driver.findElement(By.xpath(searchResults+fileNumber+"']")).isDisplayed())
        {
            String appStatus = driver.findElement(acdStatus).getText();
            status = verifyStatus(fileNumber, appStatus);
        }
        return status;
    }


    private boolean verifyStatus(String fileNumber, String appStatus) {
        boolean status;
        if((appStatus.equalsIgnoreCase("Conditioned Approval"))||
                (appStatus.equalsIgnoreCase("Conditioned Approval - Dealer Rental Fleet"))
        ||(appStatus.equalsIgnoreCase("Conditioned Approved - RP")))
        {
            status = false;
        }
        else
        {
            status = true;
            driver.findElement(By.xpath(searchResults+fileNumber+"']")).click();
            Reporter.log("Clicked on the file number "+ fileNumber);
        }
        return status;
    }
}
