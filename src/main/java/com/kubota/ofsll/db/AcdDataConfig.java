package com.kubota.ofsll.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "acdEntityManagerFactory",
        basePackages = "com.kubota.ofsll.db.repos",
        transactionManagerRef = "acdTransactionManager")
public class AcdDataConfig {

    Logger logger = LoggerFactory.getLogger(AcdDataConfig.class);

    @Primary
    @Bean
    @ConfigurationProperties("spring.datasource")
    public DataSourceProperties acdSourceProperties() {
        return new DataSourceProperties();
    }

    @Primary
    @Bean
    @ConfigurationProperties("spring.datasource.configuration")
    public DataSource acdDataSource() {
        return acdSourceProperties().initializeDataSourceBuilder().build();
    }

    @Primary
    @Bean(name = "acdEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean
    acdEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        logger.info("Creating Acd Entity Manager");
        return builder.dataSource(acdDataSource()).packages("com.kubota.ofsll.db.entities").persistenceUnit("acd").build();
    }

    @Primary
    @Bean(name = "acdTransactionManager")
    public PlatformTransactionManager acdTransactionManager(@Qualifier("acdEntityManagerFactory") EntityManagerFactory
                                                                     entityManagerFactory) {
        logger.info("Creating ACD Transaction Manager");
        return new JpaTransactionManager(entityManagerFactory);
    }

}
