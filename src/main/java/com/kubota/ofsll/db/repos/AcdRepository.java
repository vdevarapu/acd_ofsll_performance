package com.kubota.ofsll.db.repos;


import com.kubota.ofsll.db.entities.AcdEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AcdRepository extends JpaRepository<AcdEntity, String> {

   public final static String GET_XML = "select cast(XML_MSG as varchar(32672)) as  XML_MSG" +
            " from AC_XML as T where APN_APLCTN_ID = (" +
            "select APN_APLCTN_ID FROM AC_LNAPP where APN_APLCTN_NO = :acdId  AND APN_VRSN_NO = (SELECT MAX(APN_VRSN_NO) FROM AC_LNAPP WHERE APN_APLCTN_NO = :acdId AND APN_VRSN_NO IN " +
            "(SELECT APN_VRSN_NO FROM AC_LNAPP WHERE APN_APLCTN_NO = :acdId AND APN_VRSN_NO < " +
            "( select MIN(APN_VRSN_NO) FROM AC_LNAPP where APN_APLCTN_NO = :acdId AND STT_STATUS_CD = 'FFDS'" +
            "))))";


    //public final String GET_XML = "Select cast(XML_MSG as varchar(32672)) as  XML_MSG from AC_XML where APN_APLCTN_ID = :acdId";



    @Query(value = GET_XML, nativeQuery = true)
    AcdEntity findLatestById(@Param("acdId") int acdId);

}
