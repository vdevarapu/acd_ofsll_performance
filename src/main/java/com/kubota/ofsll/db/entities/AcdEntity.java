package com.kubota.ofsll.db.entities;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Component
@Entity
public class AcdEntity {

    public String getXML_MSG() {
        return XML_MSG;
    }

    public void setXML_MSG(String XML_MSG) {
        this.XML_MSG = XML_MSG;
    }

    @Id
    public String XML_MSG;

}
