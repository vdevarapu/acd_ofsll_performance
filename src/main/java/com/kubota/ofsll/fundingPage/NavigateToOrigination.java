package com.kubota.ofsll.fundingPage;

import com.kubota.ofsll.pageclass.CommonMethods;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.testng.Assert;
import org.testng.Reporter;

@Component
public class NavigateToOrigination extends CommonMethods{

    WebDriver driver;

    public WebDriver getdver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
        super.setDriver(driver);
    }

    private static final By origination = By.id("pt1:sdi1::disAcr");
    private static final By restorePane = By.xpath("//*[@id='pt1:pt_ps1::i'][@aria-label='Restore Pane']");


    public void clickOrigination() throws Exception
    {
        waitForElement(origination);
        elementClick(origination);
        waitForLoadClose();
        Reporter.log("Clicked on the Origination tab");
    }
}
