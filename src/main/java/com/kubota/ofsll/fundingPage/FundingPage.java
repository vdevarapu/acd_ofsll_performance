package com.kubota.ofsll.fundingPage;

import com.kubota.ofsll.pageclass.CommonMethods;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.testng.Assert;
import org.testng.Reporter;

import static com.kubota.ofsll.pageclass.BasePage.getDriver;

@Component
public class FundingPage extends CommonMethods{

    public WebDriver getdver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
        super.setDriver(driver);
    }

    WebDriver driver;

    private static final By fundingLink = By.id("pt1:tr1:1:cl1::text");
    private static final By appIdField = By.xpath("//input[contains(@name,'pt1:it1')]");
    private static final By appIdSubmit = By.xpath("//div[contains(@id,'pt1:rgn150:0:pt1:cb1')]/a");
    private static final By contractTab = By.xpath("//div[contains(@id,'pt1:pnt2::tabh::cbc')]/div/div/div/a[text()='Contract']");
    private static final By itemizationTab = By.xpath("//div[contains(@id,'pt1:conAcd::ti::_afrTabCnt')]/a");
    private static final By itemEditButton = By.xpath("//div[contains(@id,':pt1:rgn99:0:pt1:pt_cbtEdit')]/a");
    private static final By saveAndReturn = By.xpath("//*[@id='pt1:pt_r0:0:pt1:rgn99:1:pt1:rgn00:0:pt1:pt_cbSaveRet']/a");
    private static final By amountText = By.xpath("//*[@id='pt1:pt_r0:0:pt1:rgn99:0:pt1:pcl1:ss1it1::content']");
    private static final By approveFunded = By.xpath("//option[@title='APPROVED - FUNDED']");
    private static final By approveFundedButton = By.xpath("//*[@id='pt1:pt_r0:0:pt1:r2:3:cbSave']/a");
    private static final By fundedText = By.xpath("//*[@id='pt1:pt_r0:0:pt1:rgn0:0:pt1:pcl1:tbl1:0:c8'][text()='APPROVED - FUNDED']");
    private static final By approvedText = By.xpath("//div[contains(@id,':pt1:pcl1:tbl1::db')]/table/tbody/tr/td/div/table/tbody/tr/td[4]");
    private static final By restorePane = By.xpath("//*[@id='pt1:pt_r0:0:pt1:aplShll::ti']/div/div/a[text()='Applicant']");
    private static final By collapsePane = By.xpath("//*[@id='pt1:pt_r0:0:pt1:aplShll::ti']/div/div/a[text()='Applicant']");
    private String itemTable = "//div[contains(@id,':pt1:rgn99:0:pt1:pcl1::_ahCt')]/div/div[2]/table/tbody/tr";
    private String kccLoansrow = "]/td/div/table/tbody/tr/td[text()='AP TO KCC LOANS (MANUAL ENTRY)']";
    private String kccLoansrowAmount = "]/td/div/table/tbody/tr/td[3]";

    private String itemEditTable = "//div[@id='pt1:pt_r0:4:pt1:rgn99:1:pt1:rgn00:0:pt1:pcl1:tbl1::db']/table/tbody/tr";


    public void clickFundingLink() throws Exception
    {
        elementClick(fundingLink);
        waitForLoadClose();
        Reporter.log("Clicked on the Funding link");
    }

    public void enterAppId(String appId) throws Exception
    {
        waitForElement(appIdField);
        elementEnterText(appIdField, appId);
        Reporter.log("Entered app ID");
    }

    public void clickAppSubmit() throws Exception
    {
        elementClick(appIdSubmit);
        waitForLoadClose();
        Reporter.log("Clicked on the app ID search button");
    }

    public void clickContractTab() throws Exception
    {
        waitForElement(contractTab);
        elementClick(contractTab);
        waitForLoadClose();
        Reporter.log("Clicked on the Contract tab");
    }

    public void clickItemizationTab() throws Exception
    {
        waitForElement(itemizationTab);
        elementClick(itemizationTab);
        waitForLoadClose();
        Reporter.log("Clicked on the Itemization tab");
    }

    public void getAndUpdateAmount() throws Exception
    {
        String amount = driver.findElement(amountText).getText();
        driver.findElement(itemEditButton).click();
        Reporter.log("Clicked on the Item Edit button");
        waitForLoadClose();
        waitForElement(saveAndReturn);
        Thread.sleep(2000);
        driver.findElement(By.id("pt1:pt_r0:0:pt1:rgn99:1:pt1:rgn00:0:pt1:pcl1:tbl1:0:it1::content")).clear();
        driver.findElement(By.id("pt1:pt_r0:0:pt1:rgn99:1:pt1:rgn00:0:pt1:pcl1:tbl1:0:it1::content")).sendKeys(amount);
        driver.findElement(By.id("pt1:pt_r0:0:pt1:rgn99:1:pt1:rgn00:0:pt1:pcl1:tbl1:0:it1::content")).click();
        driver.findElement(By.id("pt1:pt_r0:0:pt1:rgn99:1:pt1:rgn00:0:pt1:pcl1:tbl1:0:it1::content")).sendKeys(Keys.TAB);
        Reporter.log("Updated the amount");
        Thread.sleep(2000);
        elementClick(saveAndReturn);
        Reporter.log("Clicked on the Save and Return button");
        waitForLoadClose();
        clickApproveFund();
    }

    public void clickApproveFund() throws Exception
    {
        Thread.sleep(2000);
        try
        {
            if(getDriver().findElement(restorePane).isDisplayed())
            {
                getDriver().findElement(restorePane).click();
                waitForElement(collapsePane);
                Thread.sleep(2000);
            }
        }catch (Exception e){}
        WebElement fundingDropDn = driver.findElement(By.id("pt1:pt_r0:0:pt1:r2:3:soc1::content"));
        if(fundingDropDn != null) {
            new Select(driver.findElement(By.id("pt1:pt_r0:0:pt1:r2:3:soc1::content")))
                    .selectByVisibleText("APPROVED - FUNDED");
            //driver.findElement(By.id("pt1:pt_r0:0:pt1:r2:3:soc1::content")).click();
            Thread.sleep(2000);
        }
        elementClick(approveFundedButton);
        waitForLoadClose();
        Thread.sleep(2000);
    }

    public String verifyFund() throws Exception
    {
        return getText(approvedText);
    }





}
