package com.kubota.ofsll.loginpage;

import com.kubota.ofsll.fundingPage.NavigateToOrigination;
import com.kubota.ofsll.pageclass.CommonMethods;
import net.bytebuddy.asm.Advice;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.testng.Assert;
import org.testng.Reporter;

@Component
public class OfsllLoginPage extends CommonMethods{

    public WebDriver getdver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
        super.setDriver(driver);
    }

    WebDriver driver;


    private static final By userNameField = By.id("pt1:_pt_it1::content");
    private static final By passwordField = By.id("pt1:_pt_it2::content");
    private static final By signInButton = By.id("pt1:_pt_cb1");
    private static final By previousLogins = By.xpath("//div[contains(text(),'You have previous open logins')]");
    private static final By previousLoginsCancel = By.xpath("//*[@id='d1_msgDlg_cancel']/a");

    public void navigateToOsfll(String url) throws Exception
    {
        driver.navigate().to(url);
    }

    public void enterUserName(String userName) throws Exception
    {
        waitForElement(userNameField);
        elementEnterText(userNameField, userName);
        driver.findElement(userNameField).sendKeys(Keys.TAB);
        Reporter.log("Entered user name "+ userName);
    }

    public void enterPassword(String password) throws Exception
    {
        elementEnterText(passwordField, password);
        driver.findElement(passwordField).sendKeys(Keys.TAB);
    }

    public void clickSignIn() throws Exception
    {
        elementClick(signInButton);
        waitForLoadClose();
        Reporter.log("Clicked on the SignIn button");
        validatePreviousLogins();
    }

    public void validatePreviousLogins()
    {
        for(int i = 0; i<100; i++)
        {
            try
            {
                if(driver.findElement(previousLogins).isDisplayed())
                {
                    driver.findElement(previousLoginsCancel).click();
                    Thread.sleep(200);
                }
            }
            catch (Exception e){

            }
        }
    }
}
